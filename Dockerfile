FROM node:12.16.1

# Create app directory
WORKDIR /usr/src/app/servises/users

COPY . ./

RUN npm install
RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "start:prod" ]

