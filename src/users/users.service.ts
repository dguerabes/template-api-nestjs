import { Inject, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  private readonly users: User[] = [];
  constructor(
    @Inject('USERS_REPOSITORY') private readonly usersRepository: typeof User,
  ) {}
  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.name = createUserDto.name;
    user.sex = createUserDto.sex;
    user.age = createUserDto.age;

    return user.save();
  }

  async findAll(): Promise<User[]> {
    return this.usersRepository.findAll<User>();
  }

  async findOne(idUser: number) {
    return this.usersRepository.findOne<User>({ where: { id: idUser } });
  }
}
