import { Test, TestingModule } from '@nestjs/testing';
import { usersProviders } from './users.providers';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { databaseProviders } from '../database/database.providers';

describe('Users Controller', () => {
  let usersService: UsersService;
  let usersController: UsersController;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService, ...usersProviders, ...databaseProviders],
    }).compile();

    usersService = moduleRef.get<UsersService>(UsersService);
    usersController = moduleRef.get<UsersController>(UsersController);
  });

  describe('getAllUser', () => {
    it('should return an array', async () => {
      const allUsers = await usersService.findAll();
      expect(Array.isArray(allUsers)).toBe(true);
    });
  });
});
