import { Sequelize } from 'sequelize-typescript';
import { User } from '../users/user.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'postgres',
        host: '',
        port: 5432,
        username: '',
        password: '',
        database: '',
        logging: false,
      });
      sequelize.addModels([User]);

      // await sequelize.sync(); // Uncomment this line to synchronise database with models

      return sequelize;
    },
  },
];
