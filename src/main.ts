import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { loggerConfiguration } from './config/loggerConfiguration';
import { swaggerConfiguration } from './config/swaggerConfiguration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(loggerConfiguration),
  });
  app.setGlobalPrefix('/api/v1');
  const document = SwaggerModule.createDocument(app, swaggerConfiguration);
  SwaggerModule.setup('/', app, document);
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
