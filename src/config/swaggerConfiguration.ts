import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfiguration = new DocumentBuilder()
  .setTitle('Microservice ...')
  .setDescription('API Template')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
